package hooks;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import wdMethods.SeMethods;

public class HooksRunner extends SeMethods{

	@Before
	public void beforeScenario(Scenario sc) {
   // System.out.println("Scenario name: "+sc.getName());
   //  System.out.println("Scenario id: "+sc.getId());
    startResult();
    startTestModule(sc.getName(), sc.getId());
    test = startTestCase(sc.getId());
	test.assignCategory("smoke");
	test.assignAuthor("Gowtham");
	startApp("chrome", "http://leaftaps.com/opentaps/control/main");
	}
	@After
	public void afterScenario(Scenario sc) {
		closeAllBrowsers();
		endResult();
	}

}