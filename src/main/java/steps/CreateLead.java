/*package steps;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver.Options;
import org.openqa.selenium.WebDriver.Window;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CreateLead {
	public ChromeDriver driver;
	@Given("Launch the browser1")
	public void launchBrowser() {
		driver = new ChromeDriver();
	}
	@And("Load the URL1")
	public void loadURL() {
		driver.get("http://leaftaps.com/opentaps");
	}
	@And("Maximize the browser1")
	public void maximize() {
		Options manage = driver.manage();window().maximize();
		Window window = manage.window();
		window.maximize();
	}
	@And("Set timeout1")
	public void timeout() {
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}
	@And("Enter the username1")
	public void typeUsername() {
		driver.findElementById("username").sendKeys("DemoSalesManager");
	}
	@And("Enter the password1")
	public void typePassword() {
		driver.findElementById("password").sendKeys("crmsfa");
	}
	@And("Click on login button1")
	public void clickLogin() {
		driver.findElementByClassName("decorativeSubmit").click();
	}	
	@And("Click CRMSFA link")
	public void clickCrmSfa() {
		driver.findElementByLinkText("CRM/SFA").click();
	}
	@And("Click Lead menu")
	public void clickLead() {
		driver.findElementByLinkText("Leads").click();
	}
	@And("Click Create Lead")
	public void clickCreateLead() {
		driver.findElementByLinkText("Create Lead").click();
	}
	@When("Type the Company name as(.*)")
	public void typeCompanyName(String data) {
		driver.findElementById("createLeadForm_companyName").sendKeys(data);
	}
	@And("Type the First name(.*)")
	public void typeFirstName(String data) {
		driver.findElementById("createLeadForm_firstName").sendKeys(data);
	}
	@And("Type the Last name(.*)")
	public void typeLastName(String data) {
		driver.findElementById("createLeadForm_lastName").sendKeys(data);
	}
	@And("Select the Source")
	public void selectSource() {		
		Select dataSource = new Select(driver.findElementById("createLeadForm_dataSourceId"));
		dataSource.selectByVisibleText("Employee");
	}
	@And("Select the Marketting Compaign")
	public void selectMarkettingCompaign() {
		Select marketingCampaign = new Select(driver.findElementById("createLeadForm_marketingCampaignId"));
		marketingCampaign.selectByValue("DEMO_MKTG_CAMP");
	}
	@And("Type the Local First name(.*)")
	public void typeLocalFirstName(String data) {
		driver.findElementById("createLeadForm_firstNameLocal").sendKeys(data);
	}
	@And("Type the Local Last name(.*)")
	public void typeLocalLastName(String data) {
		driver.findElementById("createLeadForm_lastNameLocal").sendKeys(data);
	}
	@And("Type the Salutation")
	public void typeSalutation() {
		driver.findElementById("createLeadForm_personalTitle").sendKeys("Mister");
	}
	@And("Type the Title")
	public void typeTitle() {
		driver.findElementById("createLeadForm_generalProfTitle").sendKeys("QA");
	}
	@And("Type the Department")
	public void typeDepartment() {
		driver.findElementById("createLeadForm_departmentName").sendKeys("Testing");
	}
	@And("Type the Anual revenue")
	public void typeAnualRevenue() {
		driver.findElementById("createLeadForm_annualRevenue").sendKeys("9 Billion USD");
	}
	@And("Select the Preferred Currency")
	public void selectPreferredCurrency() {
		Select currency = new Select(driver.findElementById("createLeadForm_currencyUomId"));
		currency.selectByVisibleText("USD - American Dollar");
	}
	@And("Select the Industry")
	public void selectIndustry() {
		Select industry = new Select(driver.findElementById("createLeadForm_industryEnumId"));
		
		List <WebElement> allOptions =  industry.getOptions();
		int allOptionsSize = allOptions.size();
		for(int i=0; i<allOptionsSize; i++) {
			if(i == allOptionsSize - 1) {
				//System.out.println(allOptions.get(i).getText());
				industry.selectByIndex(i);
				break;
			}
		}
	}
	@And("Type the Number Of Employee")
	public void typeNumberOfEmployee() {
		driver.findElementById("createLeadForm_numberEmployees").sendKeys("10000");
	}
	@And("Type the SIC Code")
	public void typeSICCode() {
		driver.findElementById("createLeadForm_sicCode").sendKeys("561421");
	}
	@And("Type the Ticker Symbol")
	public void typeTickerSymbol() {
		driver.findElementById("createLeadForm_tickerSymbol").sendKeys("NSE: ENTRUST");
	}
	@And("Type the Description")
	public void typeDescription() {
		driver.findElementById("createLeadForm_description").sendKeys("XYZ");
	}
	@And("Type the Important Note")
	public void typeImportantNote() {
		driver.findElementById("createLeadForm_importantNote").sendKeys("Hello");
	}
	@And("Type the Area Code")
	public void typeAreaCode() {
		driver.findElementById("createLeadForm_primaryPhoneAreaCode").sendKeys("+91");
	}
	@And("Type the Extension")
	public void typeExtension() {
		driver.findElementById("createLeadForm_primaryPhoneExtension").sendKeys("#333");
	}
	@And("Type the E-Mail Address")
	public void typeEmail() {
		driver.findElementById("createLeadForm_primaryEmail").sendKeys("gowtham3@hotmail.com");
	}
	@And("Type the Phone Number")
	public void typePhoneNumber() {
		driver.findElementById("createLeadForm_primaryPhoneNumber").sendKeys("9003746012");
	}
	@And("Type the Person to Ask For")
	public void typePersonToAsk() {
		driver.findElementById("createLeadForm_primaryPhoneAskForName").sendKeys("Gowtham");
	}
	@And("Type the Web Url(.*)")
	public void typeWebsite(String data) {
		driver.findElementById("createLeadForm_primaryWebUrl").sendKeys(data);
	}
	@And("Type the To Name(.*)")
	public void typeToName(String data) {
		driver.findElementById("createLeadForm_generalToName").sendKeys(data);
	}
	@And("Type the Attention Name(.*)")
	public void typeAttentionName(String data) {
		driver.findElementById("createLeadForm_generalAttnName").sendKeys(data);
	}
	@And("Type the Address Line 1")
	public void typeAddress1() {
		driver.findElementById("createLeadForm_generalAddress1").sendKeys("E3/10, 6th Main Road");
	}
	@And("Type the Address Line 2")
	public void typeAddress2() {
		driver.findElementById("createLeadForm_generalAddress2").sendKeys("Mogappair East");
	}
	@And("Type the City")
	public void typeCity() {
		driver.findElementById("createLeadForm_generalCity").sendKeys("Chennai");	
	}
	@And("Type the ZipPostal Code")
	public void typeZipPostalCode() {
		driver.findElementById("createLeadForm_generalPostalCode").sendKeys("600037");
	}
	@And("Type the ZipPostal Code Extension")
	public void typeZipPostalCodeExtension() {
		driver.findElementById("createLeadForm_generalPostalCodeExt").sendKeys("1234");
	}
	@And("Select the Country")
	public void selectCountry() {Select country = new Select(driver.findElementById("createLeadForm_generalCountryGeoId"));
	country.selectByVisibleText("India");
	}
	@And("Select the StateProvince")
	public void selectState() throws InterruptedException {
		Select state = new Select(driver.findElementById("createLeadForm_generalStateProvinceGeoId"));
		Thread.sleep(3000);
		state.selectByVisibleText("TAMILNADU");      
	}
	@When("Click Create Lead Button")
	public void clickCreateLeadButton() {
		driver.findElementByClassName("smallSubmit").click();
	}
	@Then("Verify Create Lead")
	public void verifyCreatedLead() {
		driver.findElementByLinkText("Lead").sendKeys();
	}
}
*/