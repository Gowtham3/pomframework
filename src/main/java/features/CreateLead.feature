Feature: Leaftaps Automation

Scenario Outline: CreateLead

When Enter the username<username>
And Enter the password<password>
And Click on login button
And Click CRMSFA link
And Click Lead menu
And Click Create Lead
When Type the Company name as<companyname>
And Type the First name<firstname>
And Type the Last name<lastname>
And Select the Source<source>
And Select the Marketting Compaign<market>
And Type the Local First name<localfirstname>
And Type the Local Last name<locallastname>
And Type the Salutation<salutation>
And Type the Title<title>
And Type the Department<department>
And Type the Anual revenue<revenue>
And Select the Preferred Currency<currency>
And Select the Industry<industry>
And Type the Number Of Employee<employee>
And Type the SIC Code<siccode>
And Type the Ticker Symbol<ticker>
And Type the Description<description>
And Type the Important Note<importantnote>
And Type the Area Code<areacode>
And Type the Extension<extension>
And Type the E-Mail Address<email>
And Type the Phone Number<mobile>
And Type the Person to Ask For<persontoask>
And Type the Web Url<website>
And Type the To Name<toname>	
And Type the Attention Name<attentionname>	
And Type the Address Line 1<address1>
And Type the Address Line 2<address2>
And Type the City<city>
And Type the ZipPostal Code<zipcode>
And Select the Country<country>
And Select the StateProvince<state>
And Click Create Lead Button
Examples:
|username|password|companyname|firstname|lastname|source|market|localfirstname|locallastname|salutation|title|department|revenue|currency|industry|employee|siccode|ticker|description|importantnote|areacode|extension|email|mobile|persontoask|website|toname|attentionname|address1|address2|city|zipcode|country|state|
|DemoSalesManager|crmsfa|CTS|Gowtham|Pethanaraj|Employee|Automobile|Gowtham|Pethanraj|Mister|QA|Testing|12 Billion|USD - American Dollar|Distribution|10000|5324|NSE: CTS|Important Lead|Welcome|91|#333|gowtham3@hotmail.com|9535006331|Gowtham|www.cts.com|Gowtham|Gowtham Pethanarj|Block-II RMZ IT Park|Sollinganallur|Chennai|60020|India|TAMILNADU|
|DemoSalesManager|crmsfa|ACCENTURE|Ramesh|Karup|Employee|Automobile|Ramesh|Karup|Mister|QA|Testing|12 Billion|USD - American Dollar|Distribution|10000|5324|NSE: ACT|Important Lead|Welcome|91|#333|ramesh3@hotmail.com|9535006331|Ramesh|www.accenture.com|Ramesh|Ramesh Karup|Block-IV RMZ IT Park|Sollinganallur|Chennai|60020|India|TAMILNADU|
