package week5.day3;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ValidateMail {

	public static void main(String[] args) {
		String email = "gowtham3@origin.com";		
		String pattern = "[\\w]+@[a-z]{4,}+.[a-z]{2,}";
		
		//String pattern = "[//w]+[//W]{1}[a-z]+[//W]{1}[a-z]";
		Pattern compile =Pattern.compile(pattern);
		Matcher matcher = compile.matcher(email);
		System.out.println("This is a valid email: "+matcher.matches());
	}

}
