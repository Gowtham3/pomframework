package week5.day3;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ValidateCreditCardNo {

	public static void main(String[] args) {
		String cardNo = "4234 2342 2314 1233";
		String pattern = "[0-9]{4}[\\s][0-9]{4}[\\s][0-9]{4}[\\s][0-9]{4}";
		Pattern compile =Pattern.compile(pattern);
		Matcher matcher = compile.matcher(cardNo);
		System.out.println("This is a valid card number: "+matcher.matches());
	}

}
