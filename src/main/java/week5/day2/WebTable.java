package week5.day2;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class WebTable {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://www.leafground.com/pages/table.html");
		WebElement table = driver.findElementByXPath("//table");
		List<WebElement> trs = table.findElements(By.tagName("tr"));
		System.out.println(trs.size());
		
//		for(WebElement rows: trs)
		for(int i=1; i<trs.size(); i++)
		{
			List<WebElement> cell = trs.get(i).findElements(By.tagName("td"));
				String progress = cell.get(1).getText();
				if(progress.equals("80%"))
				{
					//cell.get(2).findElement(By.xpath("//input[@name='vital']")).click();
					cell.get(2).click();
					break;
				}  
		}
	}

}
