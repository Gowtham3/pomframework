package week5.day2;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

public class WebTableHomeWork{
	RemoteWebDriver driver;
	public void setUp() {
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get("https://erail.in");		
		WebElement fromStation = driver.findElementById("txtStationFrom");
		fromStation.clear();
		fromStation.sendKeys("MAS",Keys.TAB);
		WebElement toStation = driver.findElementById("txtStationTo");
		toStation.clear();
		toStation.sendKeys("SBC", Keys.TAB);
		driver.findElementById("buttonFromTo").click();
		driver.findElementById("chkSelectDateOnly").click();
		WebElement trainTable = driver.findElementByXPath("//table[@class='DataTable TrainList']");
		String[] allTrainNames = getTrains(trainTable);
		Arrays.sort(allTrainNames);
		driver.findElementByLinkText("Train Name").click();
		trainTable = driver.findElementByXPath("//table[@class='DataTable TrainList']");
		String[] sortedTrains = getTrains(trainTable);
		boolean condition=false;
		for(int i =0;i<allTrainNames.length;i++) {
			if(allTrainNames[i].equals(sortedTrains[i]))
				condition = true;				
			else
				condition = false;
		}
		if(condition)
			System.out.println("Stored trains verified");
		else
			System.out.println("Sorry!!! Stored trains not verified");
		driver.close();
	}
		
	public String[] getTrains(WebElement trainTable) {
		List<WebElement> allRows = trainTable.findElements(By.tagName("tr"));
		List<String> allTrainNames = new ArrayList<>();
		for (WebElement eachRow : allRows) {
			List<WebElement> allCols = eachRow.findElements(By.tagName("td"));
			allTrainNames.add(allCols.get(1).getText());
		}
		int size = allTrainNames.size();
		String [] eachTrain = new String[size];
		for(int i = 0; i<size; i++) {
			eachTrain[i] = allTrainNames.get(i);
		}
		return eachTrain;
	}
}