package week4.collections;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

public class LearnMapConcept {
	public static void main(String arg[])
	{
		int value =1;
		String name = "Pethanaraj";
		char[] individualChar= name.toCharArray();
		Map<Character, Integer> charCount = new LinkedHashMap<>(); 
		for(char eachChar:individualChar)
		{
			if(!charCount.containsKey(eachChar))
			{
				charCount.put(eachChar, value);
			}
			else
			{
				Integer count= charCount.get(eachChar);				
				charCount.put(eachChar, count+1);
			}
		}
		for(Entry<Character, Integer> nameCount:charCount.entrySet())
		{
			System.out.println(nameCount.  getKey()+"->"+nameCount.getValue());
		}
		}
		
	}