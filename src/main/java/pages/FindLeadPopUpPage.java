package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class FindLeadPopUpPage extends ProjectMethods {

	public FindLeadPopUpPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath="//button[text()='Find Leads']")
	private WebElement eleFindLead;
	public FindLeadPopUpPage clickFindLead() {
		click(eleFindLead);
		return new FindLeadPopUpPage();
	}
	
	@FindBy(xpath="//input[@name='firstName']")
	private WebElement eleFirstName;
	public FindLeadPopUpPage typeFirstName(String fname) {
		type(eleFirstName, fname);
		return this;
	}
	
	@FindBy(xpath="(//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a)[1]")
	private WebElement eleFirstResultRecord;
	public FindLeadPopUpPage getFirstResultRecord()
	{
	firstRecord = getText(eleFirstResultRecord);
	return new FindLeadPopUpPage();
	}
	
	@FindBy(xpath="(//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a)[1]")
	private WebElement eleFirstRecord;
	public MergeLeadPage clickFirstRecord()
	{
	click(eleFirstRecord);
	switchToWindow(0);
	return new MergeLeadPage();
	}
}
