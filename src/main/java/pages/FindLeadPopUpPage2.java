package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class FindLeadPopUpPage2 extends ProjectMethods {

	public FindLeadPopUpPage2() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath="//button[text()='Find Leads']")
	private WebElement eleFindLead;
	public FindLeadPopUpPage2 clickFindLead() {
		click(eleFindLead);
		return new FindLeadPopUpPage2();
	}
	
	@FindBy(xpath="//input[@name='firstName']")
	private WebElement eleFirstName;
	public FindLeadPopUpPage2 typeFirstName(String fname) {
		type(eleFirstName, fname);
		return this;
	}

	@FindBy(xpath="(//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a)[1]")
	private WebElement eleFirstRecord;
	public MergeLeadPage clickFirstRecord()
	{
	click(eleFirstRecord);
	switchToWindow(0);
	return new MergeLeadPage();
	}
}
