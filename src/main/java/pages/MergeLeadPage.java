package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class MergeLeadPage extends ProjectMethods {

	public MergeLeadPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath="//table[@class='twoColumnForm']//img")
	private WebElement eleFromLead;
	public FindLeadPopUpPage clickFromLead() {
		click(eleFromLead);
		switchToWindow(1);
		return new FindLeadPopUpPage();
	}
	
	@FindBy(xpath="(//table[@class='twoColumnForm']//img)[2]")
	private WebElement eleToLead;
	public FindLeadPopUpPage2 clickToLead() {
		click(eleToLead);
		switchToWindow(1);
		return new FindLeadPopUpPage2();		
	}
	@FindBy(linkText="Merge")
	private WebElement eleMergeLead;
	public ViewLead clickMergeLead() {
		click(eleMergeLead);
		acceptAlert();
		return new ViewLead();		
	}

}
