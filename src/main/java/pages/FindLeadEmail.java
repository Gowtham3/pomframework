package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class FindLeadEmail extends ProjectMethods{

	
	public FindLeadEmail() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(name="emailAddress")
	private WebElement eleEmail;
	public FindLeadEmail typeEmail(String eMail)
	{
	type(eleEmail, eMail);
	return this;
	}
	
	@FindBy(xpath="//button[text()='Find Leads']")
	private WebElement eleFindLead;
	public FindLeadEmail clickFindLead()
	{
	click(eleFindLead);
	return this;
	}
	
	@FindBy(xpath="//table[@class='x-grid3-row-table']/tbody/tr/td[3]")
	private WebElement eleFirstResultRecord;
	public FindLeadEmail getFirstResultRecord()
	{
	firstRecord = getText(eleFirstResultRecord);
	return new FindLeadEmail();
	}
	
	@FindBy(xpath="(//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a)[1]")
	private WebElement eleFirstRecord;
	public ViewLead clickFirstRecord()
	{
	click(eleFirstRecord);
	return new ViewLead();
	}
	
}

