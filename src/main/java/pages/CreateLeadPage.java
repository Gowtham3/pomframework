package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import cucumber.api.java.en.When;
import wdMethods.ProjectMethods;

public class CreateLeadPage extends ProjectMethods{

	public CreateLeadPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(id="createLeadForm_companyName")
	private WebElement eleCompanyName;
	@When("Type the Company name as(.*)")
	public CreateLeadPage typeCompanyName(String companyName) {
		type(eleCompanyName, companyName);
		return this;
	}
	
	@FindBy(id="createLeadForm_firstName")
	private WebElement eleFirstName;
	@And("Type the First name(.*)")
	public CreateLeadPage typeFirstName(String firstName) {
		type(eleFirstName, firstName);
		return this;
	}

	@FindBy(id="createLeadForm_lastName")
	private WebElement eleLastName;
	@And("Type the Last name(.*)")
	public CreateLeadPage typeLastName(String lastName) {
		type(eleLastName, lastName);
		return this;
	}	

	@FindBy(id="createLeadForm_dataSourceId")
	private WebElement eleDataSource;
	@And("Select the Source(.*)")
	public CreateLeadPage selectDataSource(String dataSource) {
		selectDropDownUsingText(eleDataSource, dataSource);
		return this;
	}
	
	@FindBy(id="createLeadForm_firstNameLocal")
	private WebElement eleFirstLocalName;
	@And("Type the Local First name(.*)")
	public CreateLeadPage typeFirstLocalName(String firstLocalName) {
		type(eleFirstLocalName, firstLocalName);
		return this;
	}

	@FindBy(id="createLeadForm_personalTitle")
	private WebElement elePersonalTitle;
	@And("Type the Salutation(.*)")
	public CreateLeadPage typePersonalTitle(String personalTitle) {
		type(elePersonalTitle, personalTitle);
		return this;
	}

	@FindBy(id="createLeadForm_generalProfTitle")
	private WebElement eleProfileTitle;
	@And("Type the Title(.*)")
	public CreateLeadPage typeProfileTitle(String profileTitle) {
		type(eleProfileTitle, profileTitle);
		return this;
	}

	@FindBy(id="createLeadForm_annualRevenue")
	private WebElement eleAnnualRevenue;
	@And("Type the Anual revenue(.*)")
	public CreateLeadPage typeAnnualRevenue(String annualRevenue) {
		type(eleAnnualRevenue, annualRevenue);
		return this;
	}

	@FindBy(id="createLeadForm_industryEnumId")
	private WebElement eleIndustryId;
	@And("Select the Industry(.*)")
	public CreateLeadPage selectIndustryId(String index) {
		selectDropDownUsingText(eleIndustryId, index);
		return this;
	}
	
	@FindBy(id="createLeadForm_ownershipEnumId")
	private WebElement eleOwnershipId;
	public CreateLeadPage selectOwnershipId(String ownership) {
		selectDropDownUsingText(eleOwnershipId, ownership);
		return this;
	}
	
	@FindBy(id="createLeadForm_sicCode")
	private WebElement eleSicCode;
	@And("Type the SIC Code(.*)")
	public CreateLeadPage typeSicCode(String sicCode) {
		type(eleSicCode, sicCode );
		return this;
	}

	@FindBy(id="createLeadForm_marketingCampaignId")
	private WebElement eleMarketingCampaignId;
	@And("Select the Marketting Compaign(.*)")
	public CreateLeadPage typeMarketingCampaignId(String marketingCampaignId) {
		selectDropDownUsingText(eleMarketingCampaignId, marketingCampaignId);
		return this;
	}

	@FindBy(id="createLeadForm_lastNameLocal")
	private WebElement eleLastLocalName;
	@And("Type the Local Last name(.*)")
	public CreateLeadPage typeLastLocalName(String lastLocalName) {
		type(eleLastLocalName, lastLocalName);
		return this;
	}

	@FindBy(id="createLeadForm_departmentName")
	private WebElement eleDepartmentName;
	@And("Type the Department(.*)")
	public CreateLeadPage typeDepartmentName(String departmentName) {
		type(eleDepartmentName, departmentName);
		return this;
	}
	
	@FindBy(id="createLeadForm_numberEmployees")
	private WebElement eleNumberOfEmployees;
	@And("Type the Number Of Employee(.*)")
	public CreateLeadPage typeNumberOfEmployees(String numberOfEmployees) {
		type(eleNumberOfEmployees, numberOfEmployees);
		return this;
	}

	@FindBy(id="createLeadForm_currencyUomId")
	private WebElement eleSelectCurrency;
	@And("Select the Preferred Currency(.*)")
	public CreateLeadPage selectCurrency(String currency) {
		selectDropDownUsingText(eleSelectCurrency, currency);
		return this;
	}

	@FindBy(id="createLeadForm_tickerSymbol")
	private WebElement eleTickerSymbol;
	@And("Type the Ticker Symbol(.*)")
	public CreateLeadPage typeTickerSymbol(String tickerSymbol) {
		type(eleTickerSymbol, tickerSymbol);
		return this;
	}

	@FindBy(id="createLeadForm_description")
	private WebElement eleDescription;
	@And("Type the Description(.*)")
	public CreateLeadPage typeDescription(String description) {
		type(eleDescription, description);
		return this;
	}
	@FindBy(id="createLeadForm_importantNote")
	private WebElement eleImportantNote;
	@And("Type the Important Note(.*)")
	public CreateLeadPage typeImportantNote(String importantNote) {
		type(eleImportantNote, importantNote);
		return this;
	}

	@FindBy(id="createLeadForm_primaryPhoneAreaCode")
	private WebElement elePhoneAreaCode;
	@And("Type the Area Code(.*)")
	public CreateLeadPage typePhoneAreaCode(String phoneAreaCode) {
		type(elePhoneAreaCode, phoneAreaCode);
		return this;	
	}

	@FindBy(id="createLeadForm_primaryPhoneNumber")
	private WebElement elePhoneNumber;
	@And("Type the Phone Number(.*)")
	public CreateLeadPage typePhoneNumber(String phoneNumber) {
		type(elePhoneNumber, phoneNumber);
		return this;	
	}
	
	@FindBy(id="createLeadForm_primaryPhoneExtension")
	private WebElement elePhoneExtension;
	@And("Type the Extension(.*)")
	public CreateLeadPage typePhoneExtension(String phoneExtension) {
		type(elePhoneExtension, phoneExtension);
		return this;	
	}
	
	@FindBy(id="createLeadForm_primaryEmail")
	private WebElement elePrimaryEmail;
	@And("Type the E-Mail Address(.*)")
	public CreateLeadPage typePrimaryEmail(String primaryEmail) {
		type(elePrimaryEmail, primaryEmail);
		return this;
	}
	
	@FindBy(id="createLeadForm_primaryPhoneAskForName")
	private WebElement elePhoneAskForName ;
	@And("Type the Person to Ask For(.*)")
	public CreateLeadPage typePhoneAskForName(String phoneAskForName) {
		type(elePhoneAskForName, phoneAskForName);
		return this;
	}
	
	@FindBy(id="createLeadForm_generalToName")
	private WebElement eleToName;
	@And("Type the To Name(.*)")
	public CreateLeadPage typeToName(String toName) {
		type(eleToName, toName);
		return this;
	}
	
	@FindBy(id="createLeadForm_generalAttnName")
	private WebElement eleAttentionName;
	@And("Type the Attention Name(.*)")
	public CreateLeadPage typeAttentionName(String attentionName) {
		type(eleAttentionName, attentionName);
		return this;
	}
	
	@FindBy(id="createLeadForm_primaryWebUrl")
	private WebElement eleWebMail;
	@And("Type the Web Url(.*)")
	public CreateLeadPage typeWebMail(String webMail) {
		type(eleWebMail, webMail);
		return this;
	}

	@FindBy(id="createLeadForm_generalAddress1")
	private WebElement eleGeneralAddress1;
	@And("Type the Address Line 1(.*)")
	public CreateLeadPage typeGeneralAddress1(String generalAddress1) {
		type(eleGeneralAddress1, generalAddress1);
		return this;
	}	
	
	@FindBy(id="createLeadForm_generalAddress2")
	private WebElement eleGeneralAddress2;
	@And("Type the Address Line 2(.*)")
	public CreateLeadPage typeGeneralAddress2(String generalAddress2) {
		type(eleGeneralAddress2, generalAddress2);
		return this;
	}	
	
	@FindBy(id="createLeadForm_generalCity")
	private WebElement eleGeneralCity;
	@And("Type the City(.*)")
	public CreateLeadPage typeGeneralCity(String generalCity) {
		type(eleGeneralCity, generalCity);
		return this;
	}	
	
	@FindBy(id="createLeadForm_generalCountryGeoId")
	private WebElement eleSelectCountry;
	@And("Select the Country(.*)")
	public CreateLeadPage selectCountry(String selectCountry) {
		selectDropDownUsingText(eleSelectCountry, selectCountry);
		return this;
	}

	@FindBy(id="createLeadForm_generalStateProvinceGeoId")
	private WebElement eleSelectState;
	@And("Select the StateProvince(.*)")
	public CreateLeadPage selectState(String selectState) {
		selectDropDownUsingText(eleSelectState, selectState);
		return this;
	}
	
	@FindBy(id="createLeadForm_generalPostalCode")
	private WebElement elePostalCode;
	@And("Type the ZipPostal Code(.*)")
	public CreateLeadPage typePostalCode(String postalCode) {
		type(elePostalCode, postalCode);
		return this;
	}
	
	@FindBy(id="createLeadForm_generalPostalCodeExt")
	private WebElement eleExtPostalCode;
	@And("Type the ZipPostal Code Extension(.*)")
	public CreateLeadPage typeExtPostalCode(String extPostalCode) {
		type(eleExtPostalCode, extPostalCode);
		return this;
	}
		
	@FindBy(className="smallSubmit")
	private WebElement eleCreateLead;
	@And("Click Create Lead Button")
	public ViewLead clickCreateLead() {
		click(eleCreateLead);
		return new ViewLead();
	}
}
