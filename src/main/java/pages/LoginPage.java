package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import cucumber.api.java.en.When;
import wdMethods.ProjectMethods;

public class LoginPage extends ProjectMethods {

	public LoginPage() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(id="username")
	private WebElement eleUsername;
	@When ("Enter the username(.*)")
	public LoginPage typeUsername(String data) {
		//WebElement eleUsername = locateElement("id", "username");
		type(eleUsername, data);
		//LoginPage lp = new LoginPage();
		return this;
	}
	//@FindBy(id="password")
	//private WebElement elePassword;
	@And ("Enter the password(.*)")
	public LoginPage typePassword(String data) {
		WebElement elePassword = locateElement("id", "password");
		type(elePassword, data);
		return this;
	}
	
	@FindBy(className ="decorativeSubmit")
	private WebElement eleLogin;
	@And ("Click on login button")
	public HomePage clickLogin() {
		//WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);
		return new HomePage();
	}
}





