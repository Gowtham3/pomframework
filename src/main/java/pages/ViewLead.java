package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class ViewLead extends ProjectMethods{

	public ViewLead() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(id="viewLead_firstName_sp")
	private WebElement eleFirstName;
	public ViewLead verifyFirstName(String firstName) {		
		verifyExactText(eleFirstName, firstName);
		return this;
	}
	
	@FindBy(linkText="Delete")
	private WebElement eleDelete;
	public MyLeadsPage clickDelete() {		
		click(eleDelete);
		return new MyLeadsPage();
	}

	@FindBy(linkText="Edit")
	private WebElement eleEdit;
	public EditLeadPage clickEdit() {		
		click(eleEdit);
		return new EditLeadPage();
	}

	@FindBy(linkText="Duplicate Lead")
	private WebElement eleDuplicate;
	public DuplicateLeadPage clickDuplicate() {		
		click(eleDuplicate);
		return new DuplicateLeadPage();
	}
	
	@FindBy(linkText="Find Leads")
	private WebElement eleFindLeads;
	public FindLeadPage clickFindLeads() {		
		click(eleFindLeads);
		return new FindLeadPage();
	}

	
	@FindBy(id="viewLead_companyName_sp")
	private WebElement eleGetCompanyName;
	public ViewLead verifyUpdate(String verifyCname) {	
		verifyPartialText(eleGetCompanyName, verifyCname);
		return new ViewLead();
	}
	
	
}
