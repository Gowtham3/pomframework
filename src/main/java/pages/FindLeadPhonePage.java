package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class FindLeadPhonePage extends ProjectMethods{

	
	public FindLeadPhonePage() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(name="phoneNumber")
	private WebElement elePhoneNo;
	public FindLeadPhonePage typePhoneNo(String phoneNo)
	{
	type(elePhoneNo, phoneNo);
	return this;
	}
	@FindBy(xpath="//button[text()='Find Leads']")
	private WebElement eleFindLead;
	public FindLeadPhonePage clickFindLead()
	{
	click(eleFindLead);
	return this;
	}
	
	@FindBy(xpath="(//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a)[1]")
	private WebElement eleFirstResultRecord;
	public FindLeadPhonePage getFirstResultRecord()
	{
	firstRecord = getText(eleFirstResultRecord);
	return new FindLeadPhonePage();
	}
	
	@FindBy(xpath="(//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a)[1]")
	private WebElement eleFirstRecord;
	public ViewLead clickFirstRecord()
	{
	click(eleFirstRecord);
	return new ViewLead();
	}
	
}

