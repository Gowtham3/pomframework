package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;
public class DuplicateLeadPage extends ProjectMethods{

	public DuplicateLeadPage() {
		PageFactory.initElements(driver, this);
	}


	@FindBy(className="smallSubmit")
	private WebElement eleCreateLead;
	public ViewLead clickCreateLead() {
		click(eleCreateLead);
		return new ViewLead();
	}
	
	@FindBy(linkText="Find Leads")
	private WebElement eleFindLead;
	public FindLeadPage clickFindLead() {
		click(eleFindLead);
		return new FindLeadPage();
	}
}
