package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;
public class EditLeadPage extends ProjectMethods{

	public EditLeadPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(id="updateLeadForm_companyName")
	private WebElement eleCompanyName;
	public EditLeadPage typeCompanyName(String companyName) {
		eleCompanyName.clear();
		type(eleCompanyName, companyName);
		return new EditLeadPage();
	}

	@FindBy(xpath="//input[@type='submit']")
	private WebElement eleUpdate;
	public ViewLead clickUpdate() {
		click(eleUpdate);
		return new ViewLead();
	}
	
	@FindBy(linkText="Find Leads")
	private WebElement eleFindLead;
	public FindLeadPage clickFindLead() {
		click(eleFindLead);
		return new FindLeadPage();
	}
	
	@FindBy(linkText="Merge Leads")
	private WebElement eleMergeLead;
	public MergeLeadPage clickMergeLead() {
		click(eleMergeLead);
		return new MergeLeadPage();
	}
}
