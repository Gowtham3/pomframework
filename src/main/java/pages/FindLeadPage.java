package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class FindLeadPage extends ProjectMethods{

	public FindLeadPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath="//span[text( )='Phone']")
	private WebElement elePhoneNo;
	public FindLeadPhonePage clickPhoneNo() {
		click(elePhoneNo);
		return new FindLeadPhonePage();
	}

	@FindBy(xpath="//span[text( )='Email']")
	private WebElement eleEmail;
	public FindLeadEmail clickEmail() {
		click(eleEmail);
		return new FindLeadEmail();
	}
	
	@FindBy(name="id")
	private WebElement eleId;
	public FindLeadPage typeId() {
		type(eleId, firstRecord);
		return this;
	}

	@FindBy(xpath="//button[text()='Find Leads']")
	private WebElement eleFindLead;
	public FindLeadPage clickFindLead() {
		click(eleFindLead);
		return new FindLeadPage();
	}
	
	@FindBy(xpath="(//input[@name='firstName'])[3]")
	private WebElement eleFirstName;
	public FindLeadPage typeFirstName(String firstName) {
		type(eleFirstName, firstName);
		return new FindLeadPage();
	}
	
	@FindBy(xpath="(//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a)[1]")
	private WebElement eleFirstResultRecord;
	public FindLeadPage getFirstResultRecord()
	{
	firstRecord = getText(eleFirstResultRecord);
	return new FindLeadPage();
	}
	
	@FindBy(xpath="(//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a)[1]")
	private WebElement eleFirstRecord;
	public ViewLead clickFirstRecord()
	{
	click(eleFirstRecord);
	return new ViewLead();
	}
	
	@FindBy(xpath="x-paging-info")
	private WebElement eleVerifyMessage;
	public FindLeadPage VerifyEmptyRecord() {
		verifyExactText(eleVerifyMessage, "No Records to Display");
		return this;
	}
}
