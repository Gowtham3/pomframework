package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC005_EditLead extends ProjectMethods{
	@BeforeTest
	public void setData() {
		testCaseName="TC005_EditLead";
		testDescription="Edit the existing Lead";
		authors="Gowtham";
		category="Smoke";
		testNodes ="Edit Lead";
		dataSheetName="TC005";
	}
	@Test(dataProvider="fetchData")
	public void EditLead(String uname, String pwd, String fname, String companyName, String verifyCname) {
		new LoginPage()
		.typeUsername(uname)
		.typePassword(pwd)
		.clickLogin()
		.clickCrmSfa()
		.clickLeads()
		.clickFindLead()
		.typeFirstName(fname)
		.clickFindLead()
		.clickFirstRecord()
		.clickEdit()
		.typeCompanyName(companyName)
		.clickUpdate()
		.verifyUpdate(verifyCname);
	}
}
