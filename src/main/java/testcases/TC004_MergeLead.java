package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC004_MergeLead extends ProjectMethods{
	@BeforeTest
	public void setData() {
		testCaseName="TC004_MergeLead";
		testDescription="Merging two Lead";
		authors="Gowtham";
		category="Smoke";
		testNodes ="MergeLeads";
		dataSheetName="TC004";
	}
	@Test(dataProvider="fetchData")
	public void MergeLead(String uname, String pwd, String fname, String fname1) {
		new LoginPage()
		.typeUsername(uname)
		.typePassword(pwd)
		.clickLogin()
		.clickCrmSfa()
		.clickLeads()
		.clickMergeLead()
		.clickFromLead()
		.typeFirstName(fname)
		.clickFindLead()
		.clickFirstRecord()
		.clickToLead()
		.typeFirstName(fname1)
		.clickFindLead()
		.clickFirstRecord()
		.clickMergeLead()
		.clickFindLeads()
		.typeId()
		.clickFindLead();
		//.verifyMessage();
	}
}
