package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC006_DuplicateLead extends ProjectMethods{
	@BeforeTest
	public void setData() {
		testCaseName="TC006_DuplicateLead";
		testDescription="Duplicate the existing Lead";
		authors="Gowtham";
		category="Smoke";
		testNodes ="Duplicate Lead";
		dataSheetName="TC006";
	}
	
	@Test(dataProvider="fetchData")
	public void DuplicateLead(String uname, String pwd, String eMail) {
		new LoginPage()
		.typeUsername(uname)
		.typePassword(pwd)
		.clickLogin()
		.clickCrmSfa()
		.clickLeads()
		.clickFindLead()
		.clickEmail()
		.typeEmail(eMail)
		.clickFindLead()
		.clickFirstRecord()
		.clickDuplicate()
		.clickCreateLead();
		//.verifyFirstName();
	}
}
