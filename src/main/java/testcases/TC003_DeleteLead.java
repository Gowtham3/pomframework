package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;  

import pages.LoginPage;

import wdMethods.ProjectMethods;

public class TC003_DeleteLead extends ProjectMethods {
	
	@BeforeTest
	public void setData() {
		testCaseName="TC003_DeleteLead";
		testDescription="Creating New Lead";
		authors="Gowtham";
		category="Smoke";
		testNodes ="DeleteLeads";
		dataSheetName="TC003";
	}
	@Test(dataProvider="fetchData")
	public void DeleteLead(String uname, String pwd, String phone) {
		new LoginPage()
		.typeUsername(uname)
		.typePassword(pwd)
		.clickLogin()
		.clickCrmSfa()
		.clickLeads()
		.clickFindLead()
		.clickPhoneNo()
		.typePhoneNo(phone)
		.clickFindLead()
		.getFirstResultRecord()
		.clickFirstRecord()
		.clickDelete()
		.clickFindLead()
		.typeId()
		.clickFindLead()
		.VerifyEmptyRecord();
		}
}

