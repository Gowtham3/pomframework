package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;  

import pages.LoginPage;

import wdMethods.ProjectMethods;

public class TC002_CreateLead extends ProjectMethods {
	
	@BeforeTest
	public void setData() {
		testCaseName="TC002_CreateLead";
		testDescription="Creating New Lead";
		authors="Gowtham";
		category="Smoke";
		testNodes ="Leads";
		dataSheetName="TC002";
	}
	@Test(dataProvider="fetchData")
	public void CreateLead(String uname, String pwd, String companyName, String firstName, String lastName,
						   String dataSource, String firstLocalName, String personalTitle, String profileTitle, 
						   String annualRevenue, String index, String ownership, String sicCode, String marketingCampaignId, 
						   String lastLocalName, String departmentName, String numberOfEmployees, String currency, 
						   String tickerSymbol, String description, String importantNote, String phoneAreaCode,
						   String phoneNumber, String phoneExtension, String primaryEmail, String phoneAskForName, 
						   String toName, String attentionName, String webMail, String generalAddress1,
						   String generalAddress2, String generalCity, String selectCountry, String selectState,
						   String postalCode, String extPostalCode, String vFirstName) {
		new LoginPage()
		.typeUsername(uname)
		.typePassword(pwd)
		.clickLogin()
		.clickCrmSfa()
		.clickLeads()
		.clickCreateLead()
		.typeCompanyName(companyName)
		.typeFirstName(firstName)
		.typeLastName(lastName)
		.selectDataSource(dataSource)
		.typeFirstLocalName(firstLocalName)
		.typePersonalTitle(personalTitle)
		.typeProfileTitle(profileTitle)
		.typeAnnualRevenue(annualRevenue)
		.selectIndustryId(index) 
		.selectOwnershipId(ownership)    
		.typeSicCode(sicCode)
		.typeMarketingCampaignId(marketingCampaignId)
		.typeLastLocalName(lastLocalName)
		.typeDepartmentName(departmentName)
		.typeNumberOfEmployees(numberOfEmployees)
		.selectCurrency(currency)
		.typeTickerSymbol(tickerSymbol)
		.typeDescription(description)
		.typeImportantNote(importantNote)
		.typePhoneAreaCode(phoneAreaCode)
		.typePhoneNumber(phoneNumber)
		.typePhoneExtension(phoneExtension)
		.typePrimaryEmail(primaryEmail)
		.typePhoneAskForName(phoneAskForName)
		.typeToName(toName)
		.typeAttentionName(attentionName)
		.typeWebMail(webMail)
		.typeGeneralAddress1(generalAddress1)
		.typeGeneralAddress2(generalAddress2)
		.typeGeneralCity(generalCity)
		.selectCountry(selectCountry)
		.selectState(selectState)
		.typePostalCode(postalCode)
		.typeExtPostalCode(extPostalCode)
		.clickCreateLead()
		.verifyFirstName(vFirstName);
	}

}

